﻿using ProjektAplikacjiWeb.Models;
using System.Collections.Generic;

namespace ProjektAplikacjiWeb.ViewModels
{
    public class HomeViewModel
    {
        public IEnumerable<Book> Books { get; set; }
        public IEnumerable<Category> Categories { get; set; }
    }
}