﻿using ProjektAplikacjiWeb.Models;
using System.Collections.Generic;

namespace ProjektAplikacjiWeb.ViewModels
{
    public class EditProductViewModel
    {
        public IEnumerable<Category> Category { get; set; }
        public bool? ConfirmSuccess { get; set; }
        public Book Book { get; set; }
    }
}