﻿using ProjektAplikacjiWeb.Models;
using ProjektAplikacjiWeb.ViewModels;
using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjektAplikacjiWeb.Controllers
{
    public class ManageController : Controller
    {
        private StoreContext db = new StoreContext();

        // GET: Manage
        public ActionResult Index()
        {
            return View();
        }

        //powiązanie uprawnień zalogowanych userów z kontrolerami (atrybuty wykorzystujące role)
        [Authorize(Roles = "Admin")]
        public ActionResult AddProduct(int? bookId, bool? confirmSuccess)
        {
            if (bookId.HasValue)
                ViewBag.EditMode = true;
            else
                ViewBag.EditMode = false;

            var result = new EditProductViewModel();
            var category = db.Category.ToArray();
            result.Category = category;
            result.ConfirmSuccess = confirmSuccess;

            Book book;

            if (!bookId.HasValue)
            {
                book = new Book();
            }
            else
            {
                book = db.Book.Find(bookId);
            }

            result.Book = book;

            return View(result);
        }

        [HttpPost]
        public ActionResult AddProduct(HttpPostedFileBase file, EditProductViewModel model)
        {
            if (!ModelState.IsValid)
            {
                var category = db.Category.ToArray();
                model.Category = category;
                return View(model);
            }

            if (model.Book.BookId > 0)
            {
                //Edycja produktu
                db.Entry(model.Book).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("AddProduct", new { confirmSuccess = true });
            }
            else
            {
                //Nowy produkt

                var f = Request.Form;
                //Sprawdzenie czy dodano okładkę
                if (file != null && file.ContentLength > 0)
                {
                    //Generuje nazwę pliku okładki

                    var fileExt = Path.GetExtension(file.FileName);
                    var filename = Guid.NewGuid() + fileExt;

                    var path = Path.Combine(Server.MapPath("~/Content/Products/"), filename);
                    file.SaveAs(path);

                    model.Book.ImageFilename = filename;

                    db.Entry(model.Book).State = EntityState.Added;
                    db.SaveChanges();

                    return RedirectToAction("AddProduct", new { confirmSuccess = true });
                }
                else
                {
                    ModelState.AddModelError("", "Nie dodano okładki");
                    var category = db.Category.ToArray();
                    model.Category = category;
                    return View(model);
                }
            }
        }
    }
}