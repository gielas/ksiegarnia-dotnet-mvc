﻿using ProjektAplikacjiWeb.Models;
using ProjektAplikacjiWeb.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace ProjektAplikacjiWeb.Controllers
{
    public class HomeController : Controller
    {
        private StoreContext db = new StoreContext();

        public ActionResult Index()
        {
            //LINQ
            var categories = db.Category.ToList();
            var books = db.Book.OrderBy(x => Guid.NewGuid()).Take(6).ToList();

            var viewModel = new HomeViewModel()
            {
                Categories = categories,
                Books = books
            };

            return View(viewModel);
        }

        public ActionResult StaticContent(string viewname)
        {
            return View(viewname);
        }
    }
}