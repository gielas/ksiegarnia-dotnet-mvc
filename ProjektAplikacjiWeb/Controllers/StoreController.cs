﻿using ProjektAplikacjiWeb.Models;
using System.Linq;
using System.Web.Mvc;

namespace ProjektAplikacjiWeb.Controllers
{
    public class StoreController : Controller
    {
        StoreContext db = new StoreContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int bookId)
        {
            var book = db.Book.Find(bookId);

            return View(book);
        }

        public ActionResult Category(string categoryname)
        {
            var category = db.Category.Include("Books").Where(cat => cat.Name.ToUpper() == categoryname.ToUpper()).Single();
            var books = category.Books.ToList();

            return View(books);
        }

        [ChildActionOnly]
        public ActionResult CategoryMenu()
        {
            return PartialView("_CategoryMenu", db.Category.ToList());
        }
    }
}