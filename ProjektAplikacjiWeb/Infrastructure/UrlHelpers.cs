﻿using System.IO;
using System.Web.Mvc;

namespace ProjektAplikacjiWeb.Infrastructure
{
    //zastosowanie podstawowych helperów (wlasne helpery) 
    public static class UrlHelpers
    {
        private static readonly string bookImagePath = "~/Content/Products/";

        public static string BookImagePath(this UrlHelper helper, string imageFilename)
        {
            return helper.Content(Path.Combine(bookImagePath, imageFilename));
        }
    }
}