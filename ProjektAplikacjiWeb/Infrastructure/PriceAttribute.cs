﻿using System.ComponentModel.DataAnnotations;

namespace ProjektAplikacjiWeb.Infrastructure
{
    //atrybuty dotyczące ograniczeń danych (własne walidatory) 
    public sealed class PriceAttribute : ValidationAttribute
    {
        public override string FormatErrorMessage(string name)
        {
            return string.Format("{0} musi być większa od 0", name);
        }

        public override bool IsValid(object value)
        {
            if (value != null)
                return (decimal)value > 0;

            return true;
        }
    }
}