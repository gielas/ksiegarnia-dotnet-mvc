﻿using System.Web.Mvc;
using System.Web.Routing;

namespace ProjektAplikacjiWeb
{
    public class RouteConfig
    {
        //modyfikacja routingu RESTowego 
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "AdminAdd",
                url: "admin/edycja",
                defaults: new { controller = "Manage", action = "AddProduct" }
            );

            routes.MapRoute(
                name: "ProductDetailsCategory",
                url: "kategoria/{categoryname}/ksiazka-{bookId}",
                defaults: new { controller = "Store", action = "Details", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "StaticPage",
                url: "{viewname}",
                defaults: new { controller = "Home", action = "StaticContent", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Category",
                url: "kategoria/{categoryname}",
                defaults: new { controller = "Store", action = "Category" },
                constraints: new { categoryname = @"[\w ]+" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
