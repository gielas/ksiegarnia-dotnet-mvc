﻿using System.Collections.Generic;

namespace ProjektAplikacjiWeb.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}