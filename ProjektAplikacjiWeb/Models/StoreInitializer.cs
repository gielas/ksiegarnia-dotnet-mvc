﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;

namespace ProjektAplikacjiWeb.Models
{
    public class StoreInitializer : DropCreateDatabaseAlways<StoreContext>
    {
        protected override void Seed(StoreContext context)
        {
            SeedStoreDataCategory(context);
            SeedStoreDataBook(context);
            SeedStoreDataUser(context);
            base.Seed(context);
        }

        private void SeedStoreDataBook(StoreContext context)
        {
            var books = new List<Book>
            {
                new Book()
                {
                    BookId = 1,
                    CategoryId = 1,
                    Author = "Gaiman Neil",
                    Title = "Mitologia nordycka",
                    Description = "Wielkie nordyckie mity to jeden z korzeni, z których wyrasta nasza tradycja literacka – od Tolkiena, Alana Garnera i Rosemary Sutcliff po \"Grę o tron\" i komiksy Marvela. " +
                    "Stały się też inspiracją dla wielu obsypanych nagrodami bestsellerów Neila Gaimana. Teraz sam Gaiman sięga w odległą przeszłość, do oryginalnych źródeł tych opowieści, by przedstawić nam nowe, barwne i porywające wersje największych nordyckich historii. " +
                    "Dzięki niemu bogowie ożywają – pełni namiętności, złośliwi, wybuchowi, okrutni – a opowieść przenosi nas do ich świata – od zarania wszechrzeczy, aż po Ragnarok i zmierzch bogów." +
                    "Barwne przygody Thora, Lokiego, Odyna czy Frei fascynują współczesnego czytelnika, a żywy, błyskotliwy język sprawia, że aż proszą się o to, by czytać je na głos przy ognisku w mroźną gwiaździstą noc.",
                    ImageFilename = "1.jpg",
                    Price = 23.21m
                },
                new Book()
                {
                    BookId = 2,
                    CategoryId = 1,
                    Author = "Sapkowski Andrzej",
                    Title = "Wiedźmin. Tom 1. Ostatnie życzenie",
                    Description = "Andrzej Sapkowski, arcymistrz światowej fantasy, zaprasza do swojego Neverlandu i przedstawia uwielbianą przez czytelników i wychwalaną przez krytykę wiedźmińską sagę!" +
                    "Później mówiono, że człowiek ów nadszedł od północy, od Bramy Powroźniczej. Nie był stary, ale włosy miał zupełnie białe. Kiedy ściągnął płaszcz, okazało się, że na pasie za plecami ma miecz." +
                    "Białowłosego przywiodło do miasta królewskie orędzie: trzy tysiące orenów nagrody za odczarowanie nękającej mieszkańców Wyzimy strzygi." +
                    "Takie czasy nastały.Dawniej po lasach jeno wilki wyły, teraz namnożyło się rozmaitego paskudztwa – gdzie spojrzysz, tam upiory, bazyliszki, diaboły, żywiołaki, wiły i utopce plugawe. A i niebacznie uwolniony z amfory dżinn, potrafiący zamienić życie spokojnego miasta w koszmar, się trafi." +
                    "Tu nie wystarczą zwykłe czary ani osinowe kołki. Tu trzeba zawodowca." +
                    "Wiedźmina.Mistrza magii i miecza. Tajemną sztuką wyuczonego, by strzec na świecie moralnej i biologicznej równowagi.",
                    ImageFilename = "2.jpg",
                    Price = 36.99m
                },
                new Book()
                {
                    BookId = 3,
                    CategoryId = 1,
                    Author = "Szabałow Denis",
                    Title = "Uniwersum Metro 2033. Tom 3. Prawo do zemsty",
                    Description = "\"Prawo do zemsty\" zamyka trylogię Denisa Szabałowa w ramach Uniwersum Metro 2033." +
                    "Wcześniejsze tomy: \"Prawo do życia\" i \"Prawo do użycia siły\", zostały bardzo dobrze przyjęte przez czytelników. Szabałow i tym razem nie zawodzi!" +
                    "Wraz z książką Szabałowa czytelnicy otrzymają gratisową antologię opowiadań wyróżnionych w IV konkursie na opowiadanie z Uniwersum Glukhovsky’ego.",
                    ImageFilename = "3.jpg",
                    Price = 29.99m
                },
                new Book()
                {
                    BookId = 4,
                    CategoryId = 1,
                    Author = "Baxter Stephen, Pratchett Terry",
                    Title = "Długa Ziemia. Tom 5. Długi kosmos",
                    Description = "Lata 2070–2071. Minęło prawie sześćdziesiąt lat od Dnia Przekroczenia, a na Długiej Ziemi rozwija się nowa, postludzka spoleczność Następnych." +
                    "Dla Joshuy Valienté, dobiegającego siedemdziesiątki, nadszedł czas, by wyruszyć na jeszcze jedną, ostatnią samotną wyprawę do Wysokich Meggerów: po przygodę, która zmienia się w katastrofę." +
                    "Kiedy staje wobec bezpośredniej groźby śmierci, jedyną nadzieją ocalenia staje się stado trolli. Kiedy jednak Joshua musi się zmierzyć z własną śmiertelnością, Długa Ziemia odbiera sygnał z gwiazd " +
                    "– sygnał odebrany przez radioastronomów, a także – w sposób bardziej abstrakcyjny – przez trolle i przez wielkie trawersery. Wiadomość jest prosta, ale o porażających implikacjach: Dołączcie do nas." +
                    "Superinteligentni Następni odkrywają, że przekaz zawiera też instrukcje budowy ogromnej sztucznej inteligencji. By ją jednak skonstruować, muszą szukać pomocy w uprzemysłowionych światach ludzi." +
                    "Część po części, bajt po bajcie, montują komputer rozmiarów kontynentu – urządzenie, które zmieni miejsce Długiej Ziemi w kosmosie i ujawni ostateczny, afirmujący życie cel tych, którzy wysłali wiadomość." +
                    "Jej sens do wszystkich i odczują je wszyscy – ludzie i inne gatunki, młode i stare, społeczności i pojedyncze osoby – którzy zamieszkują Długą Ziemię.",
                    ImageFilename = "4.jpg",
                    Price = 29.49m
                },
                new Book()
                {
                    BookId = 5,
                    CategoryId = 2,
                    Author = "Robert C. Martin",
                    Title = "Czysty kod. Podręcznik dobrego programisty",
                    Description = "Poznaj najlepsze metody tworzenia doskonałego kodu" +
                    "    Jak pisać dobry kod, a zły przekształcić w dobry?" +
                    "    Jak formatować kod, aby osiągnąć maksymalną czytelność?" +
                    "    Jak implementować pełną obsługę błędów bez zaśmiecania logiki kodu?" +
                    "O tym, ile problemów sprawia niedbale napisany kod, wie każdy programista. Nie wszyscy jednak wiedzą, jak napisać ten świetny, \"czysty\" kod i czym właściwie powinien się on charakteryzować. " +
                    "Co więcej - jak odróżnić dobry kod od złego?" +
                    "Odpowiedź na te pytania oraz sposoby tworzenia czystego, czytelnego kodu znajdziesz właśnie w tej książce. Podręcznik jest obowiązkową pozycją dla każdego, kto chce poznać techniki rzetelnego i efektywnego programowania.",
                    ImageFilename = "5.jpg",
                    Price = 56.99m
                },
                new Book()
                {
                    BookId = 6,
                    CategoryId = 3,
                    Author = "Opracowanie zbiorowe",
                    Title = "Konstytucja Rzeczypospolitej Polskiej",
                    Description = "Książka zawiera pełny tekst Konstytucji Rzeczypospolitej Polskiej z dnia 2 kwietnia 1997 r." +
                    "Pozycja obowiązkowa w bibliotece wszystkich obywateli Rzeczypospolitej Polskiej.",
                    ImageFilename = "6.jpg",
                    Price = 7.99m
                },
                new Book()
                {
                    BookId = 7,
                    CategoryId = 4,
                    Author = "Young Samantha",
                    Title = "Wszystko, co w Tobie kocham",
                    Description = "Witamy ponownie w Hartwell, spokojnym, nadmorskim miasteczku, idealnym miejscu, gdzie można uciec od wszystkiego i znaleźć uczucie, na które wcale nie czekałaś…" +
                    "Bailey Hartwell ma wiele powodów do zadowolenia – odnosi sukcesy w interesach, ma grono oddanych przyjaciół i stałego chłopaka, chociaż po dziesięciu latach ich niezobowiązujący związek wydaje się nieco nudny i skostniały. " +
                    "Jedynym niepokojącym elementem w jej życiu jest przystojny biznesmen Vaughn Tremaine.Bailey uważa, że ten przybysz z Nowego Jorku patrzy na wszystkich z góry, a ją uważa za prowincjonalną gęś." +
                    "Jednak kiedy doświadcza niespodziewanej zdrady ze strony bliskiej osoby, zaskoczona stwierdza, że Vaughn to całkiem przyzwoity facet." +
                    "Vaughn podziwia Bailey za fantazję, niezależność i lojalność.Im więcej budzi w nim namiętności, tym częściej wchodzi z nią w konflikty. " +
                    "Każdy jej gest i słowo działa na niego jak afrodyzjak. Jednak dramatyczne przeżycia z przeszłości sprawiają, ze Vaughn postanawia odejść, bojąc się, że ją skrzywdzi. " +
                    "Bailey, która przeżyła w życiu zbyt wiele rozczarowań, także rezygnuje z walki, wbrew swojej naturze." +
                    "Kiedy Vaughn zdaje sobie sprawę, że popełnił największy błąd swojego życia, postanawia za wszelką cenę przekonać Bailey, że taka miłość, jaka ich połączyła, zdarza się tylko raz w życiu.",
                    ImageFilename = "7.jpg",
                    Price = 25.99m
                },
                new Book()
                {
                    BookId = 8,
                    CategoryId = 5,
                    Author = "Paris B.A.",
                    Title = "Za zamkniętymi drzwiami",
                    Description = "Perfekcyjna para? Doskonałe małżeństwo? Czy idealne kłamstwo?" +
                    "Wszyscy znamy takie pary jak Jack i Grace: on jest przystojny i bogaty, ona czarująca i elegancka. Chciałoby się poznać Grace nieco lepiej, ale to niełatwe, bo Jack i Grace są nierozłączni." +
                    "Niektórzy nazwaliby to prawdziwą miłością. Wyobraź sobie uroczystą kolację w ich idealnym domu, miłą konwersację, kolejne kieliszki dobrego wina. Oboje wydają się w swoim żywiole. " +
                    "Przyjaciele Grace chcieliby zrewanżować się lunchem w przyszłym tygodniu.Ona chętnie przyjęłaby zaproszenie, ale wie, że nigdy z nimi nie wyjdzie. " +
                    "Ktoś mógłby spytać, dlaczego Grace nigdy nie odbiera telefonów, nie wychodzi z domu, a nawet nie pracuje. I jak to możliwe, że gotując tak wymyślne potrawy, w ogóle nie tyje? I dlaczego w oknach sypialni są kraty?" +
                    "Doskonałe małżeństwo czy perfekcyjne kłamstwo ?",
                    ImageFilename = "8.jpg",
                    Price = 28.49m
                },
                new Book()
                {
                    BookId = 9,
                    CategoryId = 6,
                    Author = "Elsberg Marc",
                    Title = "Blackout",
                    Description = "Pewnego zimowego dnia w całej Europie następuje przerwa w dostawie prądu – pełne zaciemnienie. Włoski informatyk i były haker Piero Manzano podejrzewa, że może to być zmasowany elektroniczny atak terrorystyczny. " +
                    "Próbując ostrzec władze, sam zostaje uznany za podejrzanego. " +
                    "W próbie rozwiązania zagadki stara się mu pomóc dziennikarka Lauren Shannon. " +
                    "Im bliżej będą prawdy o przyczynie zaistniałej sytuacji, tym większe ich zaskoczenie oraz niebezpieczeństwo, na jakie się narażają." +
                    "Tymczasem Europa pogrąża się w ciemności. Zaczyna brakować podstawowych środków do życia: wody, jedzenia, ogrzewania.Wystarczy kilka dni, by zapanował chaos na niespotykaną skalę." +
                    "Thriller naukowy „Blackout” (i powieść sensacyjna w jednym) realistycznie przedstawia prawdziwie czarny scenariusz wydarzeń, których prawdopodobieństwo jest tym większe, im bardziej nasze codzienne życie uzależnione jest od elektroniki. " +
                    "Dopóki jest prąd, jesteś bezpieczny…" +
                    "Czy jesteśmy przygotowani na blackout ? ",
                    ImageFilename = "9.jpg",
                    Price = 36.99m
                },
            };

            books.ForEach(book => context.Book.Add(book));
            context.SaveChanges();
        }

        private void SeedStoreDataCategory(StoreContext context)
        {
            var categories = new List<Category>
            {
                new Category() { CategoryId = 1, Name = "Fantastyka" },
                new Category() { CategoryId = 2, Name = "Informatyka" },
                new Category() { CategoryId = 3, Name = "Prawo" },
                new Category() { CategoryId = 4, Name = "Romans" },
                new Category() { CategoryId = 5, Name = "Kryminał" },
                new Category() { CategoryId = 6, Name = "Horror" },
            };

            categories.ForEach(category => context.Category.Add(category));
            context.SaveChanges();
        }

        private void SeedStoreDataUser(StoreContext context)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            const string name = "s10755@pjwstk.edu.pl";
            const string password = "zaq1@WSX";
            const string roleName = "Admin";

            var user = userManager.FindByName(name);
            if (user == null)
            {
                user = new ApplicationUser { UserName = name, Email = name };
                var result = userManager.Create(user, password);
                result = userManager.SetLockoutEnabled(user.Id, false);
            }

            //Create Role Admin if it does not exist
            var role = roleManager.FindByName(roleName);
            if (role == null)
            {
                role = new IdentityRole(roleName);
                var roleresult = roleManager.Create(role);
            }

            // Add user admin to Role Admin if not already added
            var rolesForUser = userManager.GetRoles(user.Id);
            if (!rolesForUser.Contains(role.Name))
            {
                var result = userManager.AddToRole(user.Id, role.Name);
            }
        }
    }
}