﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace ProjektAplikacjiWeb.Models
{
    public class StoreContext : IdentityDbContext<ApplicationUser>
    {
        public StoreContext() : base("StoreContext") { }
        static StoreContext() { Database.SetInitializer(new StoreInitializer()); }

        public DbSet<Book> Book { get; set; }
        public DbSet<Category> Category { get; set; }

        public static StoreContext Create()
        {
            return new StoreContext();
        }
    }
}