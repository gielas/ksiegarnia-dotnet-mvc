﻿using ProjektAplikacjiWeb.Infrastructure;
using System.ComponentModel.DataAnnotations;

namespace ProjektAplikacjiWeb.Models
{
    public class Book
    {
        public int BookId { get; set; }
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "Wprowadź nazwę autora")]
        [Display(Name = "Autor")]
        public string Author { get; set; }

        [Required(ErrorMessage = "Wprowadź tytuł")]
        [Display(Name = "Autor")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Wprowadź cenę")]
        [Price]
        [Display(Name = "Cena")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Wprowadź opis")]
        [Display(Name = "Opis")]
        public string Description { get; set; }

        public string ImageFilename { get; set; }

        //minimum 2 tabele z relacją jeden-do-wiele
        public virtual Category Category { get; set; }
    }
}